# Personnes CRUD APP

Test Technique Managoe

## Pour tester l'application

- Cloner ce repo
- se placer dans le dossier root du projet 
- Exécuter les commandes suivantes :

```shell
npm install
```

```shell
npm start
```

- ouvrir un navigateur web puis aller à l'adresse `http://localhost:3000`


## Architecture de l'application

L'Application CRUD se structure de la façon suivante:

`Le service FakeService.js`

- Utilisation de Promises pour permettre des appels asynchrones vers le service (comme en http)

- Les données exposées par le Service ne sont pas persistées  


`Structure des Composants`

Le découpage en composants de l'appli se présente comme suit :

- Un composant Navbar qui affiche le menu
- Un composant add-personne qui affiche un formulaire pour l'ajout d'une nouvelle personne (C)
- Un composant personne-list qui affiche la liste des personnes et les détails de la personne sélectionée  (R)
- Un composant personne qui affiche un formulaire permettant de modifier les détails d'une personne ou de supprimer une personne (UD)