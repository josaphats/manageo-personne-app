import { Component } from "react";
import FakeService from "../../services/FakeService";
import "bootstrap/dist/css/bootstrap.min.css";

import "./personne.scss";


export default class Personne extends Component {

    constructor(props) {
        super(props);
        this.onChangeNom = this.onChangeNom.bind(this);
        this.onChangePrenom = this.onChangePrenom.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.getPersonne = this.getPersonne.bind(this);
        this.updatePersonne = this.updatePersonne.bind(this);
        this.deletePersonne = this.deletePersonne.bind(this);

        this.state = {
            currentPersonne: {
                id: null,
                nom: "",
                prenom: "",
                email: "",
            },
            message: ""
        };
    }

    componentDidMount() {
        this.getPersonne(this.props.match.params.id);
        
    }

    onChangeNom(e) {
        const nom = e.target.value;

        this.setState(function (prevState) {
            return {
                currentPersonne: {
                    ...prevState.currentPersonne,
                    nom: nom
                }
            };
        });
    }

    onChangePrenom(e) {
        const prenom = e.target.value;

        this.setState(prevState => ({
            currentPersonne: {
                ...prevState.currentPersonne,
                prenom: prenom
            }
        }));
    }

    onChangeEmail(e) {
        const email = e.target.value;

        this.setState(prevState => ({
            currentPersonne: {
                ...prevState.currentPersonne,
                email: email
            }
        }));
    }

    getPersonne(id) {
        FakeService.get(id)
            .then(response => {
                this.setState({
                    currentPersonne: response
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    updatePersonne() {
        FakeService.update(
            this.state.currentPersonne.id,
            this.state.currentPersonne
        )
            .then(response => {
                console.log(response);
                this.setState({
                    message: "Les détails de la personne ont été mis à jour"
                });

                setTimeout(() => {
                    this.setState({
                        message: ""
                    });

                }, 4000)


            })
            .catch(e => {
                console.log(e);
            });
    }

    deletePersonne() {
        FakeService.delete(this.state.currentPersonne.id)
            .then(response => {
                console.log(response);
                this.props.history.push('/personnes')
            })
            .catch(e => {
                console.log(e);
            });
    }


    render() {
        const { currentPersonne } = this.state;

        return (
            <div>
                {currentPersonne ? (
                    <div className="edit-form">
                        <h4>{currentPersonne.prenom} {currentPersonne.nom}</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="nom-edit">Nom</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="nom-edit"
                                    value={currentPersonne.nom}
                                    onChange={this.onChangeNom}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="prenom-edit">Prénom</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="prenom-edit"
                                    value={currentPersonne.prenom}
                                    onChange={this.onChangePrenom}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="email-edit">Email</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="email-edit"
                                    value={currentPersonne.email}
                                    onChange={this.onChangeEmail}
                                />
                            </div>

                        </form>

                
                        
                        <button
                            type="submit"
                            className="update-btn btn btn-sm btn-success"
                            onClick={this.updatePersonne}
                        >
                            Mettre à jour
                        </button>

                        <button
                            className="delete-btn btn btn-sm btn-danger"
                            onClick={this.deletePersonne}
                        >
                            Supprimer
                        </button>

                        <p className="update-message">{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Veuillez selectionner une personne...</p>
                        </div>
                    )}
            </div>
        );

    }




}