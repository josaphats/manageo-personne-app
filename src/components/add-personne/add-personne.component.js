import { Component } from "react";
import FakeService from "../../services/FakeService";
import "./add-personne.scss";

export default class AddPersonne extends Component { 
    constructor(props) {
        super(props);

        this.onChangePrenom = this.onChangePrenom.bind(this);
        this.onChangeNom = this.onChangeNom.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.savePersonne = this.savePersonne.bind(this);
        this.newPersonne = this.newPersonne.bind(this);

        this.state = {
            id: null,
            prenom: "",
            nom: "", 
            email: "",
            published: false,

            submitted: false
        };
        
    }

    onChangeNom(e) {
        this.setState({
          nom: e.target.value
        });
      }
    
    onChangePrenom(e) {
        this.setState({
          prenom: e.target.value
        });
    }

    onChangeEmail(e) {
        this.setState({
          email: e.target.value
        });
    }
    
    savePersonne() {
        var data = {
          nom: this.state.nom,
          prenom: this.state.prenom,
          email: this.state.email
        };
    
        FakeService.create(data)
          .then(response => {
            this.setState({
              id: response.id,
              nom: response.nom,
              prenom: response.prenom,
              email: response.email,    
              submitted: true
            });
            console.log(response);
          })
          .catch(e => {
            console.log(e);
          });
      }
    
      newPersonne() {
        this.setState({
          id: null,
          nom: "",
          prenom: "",
          email: "",    
          submitted: false
        });
      }

    render() {
        return (
            <div className="submit-form">
            {this.state.submitted ? (
              <div className="confirmation-ajout">
                <h4>Personne correctement ajoutée</h4>
                <button className="btn btn-success" onClick={this.newPersonne}>
                  Ajouter une autre personne
                </button>
              </div>
            ) : (
              <div>
                <div className="form-group">
                  <label htmlFor="nom">Nom</label>
                  <input
                    type="text"
                    className="form-control"
                    id="nom"
                    required
                    value={this.state.nom}
                    onChange={this.onChangeNom}
                    name="nom"
                  />
                </div>
    
                <div className="form-group">
                  <label htmlFor="prenom">Prénom</label>
                  <input
                    type="text"
                    className="form-control"
                    id="prenom"
                    required
                    value={this.state.prenom}
                    onChange={this.onChangePrenom}
                    name="prenom"
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input
                    type="text"
                    className="form-control"
                    id="email"
                    required
                    value={this.state.email}
                    onChange={this.onChangeEmail}
                    name="email"
                  />
                </div>
    
                <button onClick={this.savePersonne} className="btn btn-success">
                  Envoyer
                </button>
              </div>
            )}
          </div>
        );
    }
    

}