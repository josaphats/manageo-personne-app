import { Link } from "react-router-dom";
import "./navbar.scss";


export default function Navbar (props) {

        return    <nav className="navbar navbar-expand navbar-dark bg-dark">
                    <a href="/personnes" className="navbar-brand">
                        Manageo Personnes CRUD app
                    </a>
                    <div className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={"/personnes"} className="nav-link">
                                Personnes
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/add"} className="nav-link">
                                Nouvelle Personne
                            </Link>
                        </li>
                    </div>
                </nav>
         
}

