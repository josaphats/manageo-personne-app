import { Component } from "react";
import { Link } from "react-router-dom";
import FakeService from "../../services/FakeService";
import "bootstrap/dist/css/bootstrap.min.css";

import "./personne-list.scss";


export default class PersonneList extends Component {

    constructor(props) {
        super(props);
        this.retrievePersonnes = this.retrievePersonnes.bind(this);
        this.setActivePersonne = this.setActivePersonne.bind(this);

        this.state = {
            personnes: [],
            currentPersonne: null,
            currentIndex: -1,
        };
    }


    componentDidMount() {
        this.retrievePersonnes();
    }

    retrievePersonnes() {
        FakeService.getAll()
            .then(response => {
                this.setState({
                    personnes: response
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }


    setActivePersonne(personne, index) {
        this.setState({
            currentPersonne: personne,
            currentIndex: index
        });
    }


    render() {
        const { personnes, currentPersonne, currentIndex } = this.state;

        return (
            <div className="list row">
              
                <div className="col-md-6">
                    <h4>Liste des personnes</h4>

                    <ul className="list-group">
                        {personnes &&
                            personnes.map((personne, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActivePersonne(personne, index)}
                                    key={index}
                                >
                                    {personne.nom}
                                </li>
                            ))}
                    </ul>

                </div>
                <div className="col-md-6">
                    {currentPersonne ? (
                        <div className="personne-details">
                            <h4>Détails de la sélection</h4>
                            <div>
                                <label>
                                    <strong>Nom:</strong>
                                </label>{" "}
                                {currentPersonne.nom}
                            </div>
                            <div>
                                <label>
                                    <strong>Prenom:</strong>
                                </label>{" "}
                                {currentPersonne.prenom}
                            </div>
                            <div>
                                <label>
                                    <strong>Email:</strong>
                                </label>{" "}
                                {currentPersonne.email}
                            </div>

                            <Link
                                to={"/personnes/" + currentPersonne.id}
                            >
                                    Mettre à jour ou Supprimer
                            </Link>
                        </div>
                    ) : (personnes.length > 0) ? (
                            <div className="personne-details">
                                <br />
                                <p>Veuillez sélectionner une personne...</p>
                            </div>
                        ) : ""}
                </div>
            </div>
        );

    }
}