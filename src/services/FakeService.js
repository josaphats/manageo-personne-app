class FakeService {

    personnes_list = [
                            { id: 1, prenom: "Fizz", nom: "FizzBuzz", email: "fizzbuzz@test.fr" },
                            { id: 2, prenom: "Josaphat", nom: "SOME", email: "phat@test.fr" },
                            { id: 3, prenom: "Foo", nom: "FooBar", email: "foobar@test.fr" }
                         ]

    // Pour simplifier, on attribue les nouvelles id de façon incrémentale sans tenir compte des suppressions
    last_personne_id = 3;

    getAll() {

        return new Promise((resolve, reject) => {
            resolve (this.personnes_list)
        });
    }
  
    get(id) {
       
        return new Promise((resolve, reject) => {
                for (let i=0; i < this.personnes_list.length; i++) {
                    if (this.personnes_list[i].id == id) {
                        resolve(this.personnes_list[i]);
                    }
                }
        });
    }
  
    create(personne) {
        // Pour simplifier, on attribue l'id de façon incrémentale pour simplifier
        let newPersonne = {
            id: this.last_personne_id + 1,
            prenom: personne.prenom,
            nom: personne.nom,
            email: personne.email
        }
        this.personnes_list.push(newPersonne);
        this.last_personne_id = newPersonne.id;
        return new Promise((resolve, reject) => {
            resolve(newPersonne);
        });
    }
  
    update(id, personne) {
        return new Promise ((resolve, reject) => {
            for (let i=0; i < this.personnes_list.length; i++) {
                if (this.personnes_list[i].id == id) {
                    this.personnes_list[i].nom = personne.nom;
                    this.personnes_list[i].prenom = personne.prenom;
                    this.personnes_list[i].email = personne.email;
                    resolve(this.personnes_list[i]);
                }
            }
        });
    }
  
    delete(id) {
        return new Promise ((resolve, reject) => {
            let flag = 0;
            for (let i=0; i < this.personnes_list.length; i++) {
                if (this.personnes_list[i].id == id) {
                    let copy = {...this.personnes_list[i]};
                    this.personnes_list.splice(this.personnes_list.indexOf(this.personnes_list[i]), 1);
                    flag = 1;
                    resolve(copy);
                }
            }
            if(flag = 0) {
                reject("la personne n'existe pas");
            }
        })
    }
  
    
  }
  
  export default new FakeService();