import { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from './components/navbar/navbar.component';
import "bootstrap/dist/css/bootstrap.min.css";
import './App.scss';

import AddPersonne from "./components/add-personne/add-personne.component";
import Personne from "./components/personne/personne.component";
import PersonneList from "./components/personne-list/personne-list.component";


class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />

        <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/personnes"]} component={PersonneList} />
              <Route exact path="/add" component={AddPersonne} />
              <Route path="/personnes/:id" component={Personne} />
            </Switch>
        </div>

      </div>
    );
  }
}

export default App;
